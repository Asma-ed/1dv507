package count_words;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

public class WordCount2Main {

    static String filePath = "C:\\Users\\zurg2\\OneDrive\\Desktop\\HomeWork\\Assignmen4\\Ex1\\Words.txt";

    static Scanner fileReader;
    static HashWordSet wordHashSet = new HashWordSet();
    static TreeWordSet wordTreeSet = new TreeWordSet();

    public static void main(String[] args) {

        try {
            File txtFile = new File(filePath);
            fileReader= new Scanner(txtFile);

            while (fileReader.hasNext()){

                String readWord= fileReader.next();

                Word word = new Word(readWord);

                wordHashSet.add(word);
                wordTreeSet.add(word);

            }

            System.out.println("Hash set size = " +wordHashSet.size());
            System.out.println("Tree set size = " +wordTreeSet.size());

            int numberOfWords = 1;
            System.out.println("HashTree set Iterator : ");

            Iterator<Word> it = wordTreeSet.iterator();
            while (it.hasNext()) {
                System.out.println(numberOfWords + " :" + it.next());
                numberOfWords++;
            }

        }


        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

package count_words;

import java.io.*;

public class IdentyfyWordsMain {



    public static void main(String[] args) throws IOException {

     readAndWriteTxt("src/count_wordscount_words/HistoryOfProgramming.txt");

    }


    public static void readAndWriteTxt(String filePath) throws IOException {

        File txtFile = new File(filePath);

        StringBuilder builder = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(txtFile));

        String st;
        while ((st = br.readLine()) != null) {

            st= st.replaceAll("\\-"," ");
            st = st.replaceAll("[^a-zA-Z\\s]","");
            builder.append(st+"\n");


        }

        File newFile = new File(txtFile.getParentFile() + "/count_words/words.txt");
        newFile.createNewFile();
        PrintWriter printer = new PrintWriter(newFile);
        printer.print(builder.toString());
        printer.close();
    }
}

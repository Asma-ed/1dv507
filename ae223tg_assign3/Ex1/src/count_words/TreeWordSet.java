package count_words;

import java.util.Iterator;

public class TreeWordSet implements WordSet {

    private BinaryTree root;
    private BinaryTree lastLeaf;
    private BinaryTree firstLeaf;
    private StringBuilder builder = new StringBuilder();
    public Word[] rootArray =  new Word[8];



    private int treeSize;

    class BinaryTree{

        public Word word;
        public BinaryTree right ;
        public BinaryTree left ;


        public BinaryTree(Word word) {
            this.word = word;
            right= null;
            left = null;


        }

    }


    @Override
    public void add(Word word) {

        BinaryTree leaf = new BinaryTree(word);
        if(root == null){
            root = leaf;
            lastLeaf = root;
            rootArray[treeSize]= word;
            treeSize++;
        }

        else if (!contains(word)){

                if(word.compareTo(lastLeaf.word)<0){

                    lastLeaf.left = new BinaryTree(word);
                    rootArray[treeSize]=word;
                    treeSize++;
                    if (treeSize==rootArray.length){
                        resize();
                    }

                }
                else if (word.compareTo(lastLeaf.word)>0){


                        lastLeaf.right = new BinaryTree(word);
                        rootArray[treeSize]=word;
                        treeSize++;
                    if (treeSize==rootArray.length){
                        resize();
                    }

                }

            }
        }

    private void resize() {
        Word[] temp= rootArray;
        rootArray = new Word[temp.length*2];
        for (int i = 0 ; i<temp.length; i++){
            rootArray[i]= temp[i];
        }
    }


    @Override
    public boolean contains(Word word) {
        if(root == null){
            return false;
        }

        firstLeaf = root;
        while (firstLeaf!=null){

            lastLeaf = firstLeaf;
            if(word.compareTo(firstLeaf.word)<0){

                firstLeaf = firstLeaf.left;
            }
            else if(word.compareTo(firstLeaf.word)>0){

                firstLeaf = firstLeaf.right;
            }
            else if (word.compareTo(firstLeaf.word)==0){

                return true;
            }

        }

        return false;

    }

    @Override
    public int size() {
        return treeSize;
    }

    @Override
    public String toString() {
        if (root == null){

            return "Tree is empty";
        }

        else {

          return  helpToString(root);
        }
    }

    private String helpToString (BinaryTree leaf){
        if (leaf!= null){

            helpToString(leaf.left);
            builder.append(leaf.word.toString()+ ", ");
            helpToString(leaf.right);
        }

        return builder.toString();

    }




    @Override
    public Iterator<Word> iterator() {
        return new TreeIterator();
    }

    class TreeIterator implements Iterator<Word>{


        private int pos = 0;
        private int count=0;


        public TreeIterator(){
            treeArray();
        }

        @Override
        public boolean hasNext() {
            if(pos<rootArray.length) {
                if (root == null) {
                    return false;
                }
                if (rootArray[pos] == null) {
                    return false;
                }
            }

            return pos<rootArray.length;
            }

        @Override
        public Word next() {
            return rootArray[pos++];
        }



        private void treeArray(){
            if (root == null){
                throw new NullPointerException();
            }

            else {
                rootArray = new Word[treeSize];

                helpTreeArray(root);
            }

        }

        private void helpTreeArray(BinaryTree leaf) {

            if (leaf!= null){

               helpTreeArray(leaf.left);
                rootArray[count]= leaf.word;
                count++;
                helpTreeArray(leaf.right);
            }



        }


    }


    }






package count_words;

import java.util.Iterator;

public class HashWordSet implements WordSet {

    private int hashSetSize;
    private Node[] nodeArray = new Node[8];
    private Node lastNode; //Last visited node

    class Node {

        Word word;
        Node nextNode;

        public Node(Word word) {

            this.word = word;
            nextNode = null;
        }

    }


    @Override
    public void add(Word word) {

        int hashNum = getHashNumber(word);

        if (!contains(word)) {
            lastNode = new Node(word);
            lastNode.nextNode = nodeArray[hashNum];
            nodeArray[hashNum] = lastNode;
            hashSetSize++;
            if (hashSetSize == nodeArray.length)
                rehash();
        }


    }


    private void rehash() {
        hashSetSize = 0;

        Node[] temp = nodeArray;

        nodeArray = new Node[2 * temp.length];

        for (Node node : temp) {

            while (node != null) {
                add(node.word);
                node = node.nextNode;
            }

        }
    }

    @Override
    public boolean contains(Word word) {
        int hashNum = getHashNumber(word);

        lastNode = nodeArray[hashNum];

        while (lastNode != null) {
            if (lastNode.word.equals(word))
                return true;
            else {
                lastNode = lastNode.nextNode;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return hashSetSize;
    }

    private int getHashNumber(Word word) {

        int hashNum = word.hashCode();

        return hashNum % nodeArray.length;


    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[ ");

        for (Node node : nodeArray) {
            while (node != null) {
                builder.append(node.word + " ");
                node = node.nextNode;
            }
        }
        builder.append("]");
        return builder.toString();
    }

    @Override
    public Iterator<Word> iterator() {
        return new HashIterator();
    }

    class HashIterator implements Iterator<Word> {


        private Node node;
        private int hashNum;

        public HashIterator() {
            node = null;
            hashNum = 0;
        }

        @Override
        public boolean hasNext() {


            if (node != null && node.nextNode != null) {
                return true;
            }

            for (int i = hashNum; i < nodeArray.length; i++) {
                if (nodeArray[i] != null) {
                    hashNum= i+1;
                    return true;
                }
            }
            return false;
        }

        @Override
        public Word next() {
            if (node != null && node.nextNode != null) {
                node = node.nextNode;
            }
            else {
                node = nodeArray[hashNum-1];

            }

            return node.word;
        }

        public int getHashNum() {
            return hashNum;
        }
    }

}

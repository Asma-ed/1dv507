package time;

import java.util.Comparator;
import java.util.Random;

public class SortTime {

    private final static long SECOND = 1000000000;
    private final static long MilliSECOND = 1000000;

    public static void main(String[] args) {

sortString();

    }

    private static void sortInteger(){
       int numOfElements =1500;
        int[] unsortedArray= randomIntArrayGenerator(numOfElements);
       long time=0;
        while (time<1000){


            long timerStart = System.nanoTime();
            insertionSort(unsortedArray);
            long timerEnd = System.nanoTime();

             time = (timerEnd-timerStart)/MilliSECOND;


             if (time<950){

                 unsortedArray=randomIntArrayGenerator(unsortedArray.length+1000);
             }

             else if (time>1100){
                 unsortedArray=randomIntArrayGenerator(unsortedArray.length-100);
                 time=0;
             }

             else if (time>1000&&time<1100){
                 unsortedArray=randomIntArrayGenerator(unsortedArray.length-1);
                 time=0;

             }
             else if (time==1000){
                 break;
             }

             else {
                 unsortedArray=randomIntArrayGenerator(unsortedArray.length+50);
                 time=0;

             }
        }
        System.out.println(" Time : "+time+" number of elements  : "+unsortedArray.length);



    }

    private static void sortString(){
        int numOfElements =10;
        String [] unsortedArray= randomStringArrayGenerator(numOfElements);
        long time=0;
        while (time<1000){


            long timerStart = System.nanoTime();
            insertionSort(unsortedArray,Comparator.naturalOrder());
            long timerEnd = System.nanoTime();

            time = (timerEnd-timerStart)/MilliSECOND;


            if (time<950){

                unsortedArray=randomStringArrayGenerator(unsortedArray.length+1000);
            }

            else if (time>1100){
                unsortedArray=randomStringArrayGenerator(unsortedArray.length-100);
                time=0;
            }

            else if (time>1000&&time<1100){
                unsortedArray=randomStringArrayGenerator(unsortedArray.length-1);
                time=0;

            }
            else if (time==1000){
                break;
            }

            else {
                unsortedArray=randomStringArrayGenerator(unsortedArray.length+50);
                time=0;

            }
        }
        System.out.println(" Time : "+time+" number of elements  : "+unsortedArray.length);



    }

    private static int[] randomIntArrayGenerator(int size){

        Random random = new Random();
        int[] result = new int[size];

        for (int i = 0 ; i<result.length;i++){

            result[i]= random.nextInt(size*2);
        }

        return result;


    }

    private static String[] randomStringArrayGenerator(int size){

        Random randomLetter = new Random();


        String[] result = new String [size];

        for (int i = 0 ; i<result.length;i++){
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j<10;j++){
                builder.append((char)(randomLetter.nextInt(26) + 'a'));
            }
            result[i]=builder.toString();
        }

        return result;


    }





    private static int[] insertionSort(int[] in){

        if(in.length==0){

            throw new IllegalArgumentException("Array is empty");
        }

        int[] sortedArray = in.clone();



        for (int i = 1 ; i< sortedArray.length ; i++){

            int testKey = sortedArray[i];
            int j = i-1;

            while (j>-1 &&  sortedArray[j]> testKey){

                sortedArray[j+1]= sortedArray[j];
                j--;
            }

            sortedArray[j+1]= testKey;

        }
        return sortedArray;

    }

    public static String[] insertionSort (String[] in, Comparator<String> c){

        if( in.length==0){

            throw new IllegalArgumentException("Array is empty");
        }

        String[] sortedArray = in.clone();



        for (int i = 1 ; i< sortedArray.length ; i++){

            String testKey = sortedArray[i];
            int j = i-1;


            while (j>-1 && c.compare(sortedArray[j],testKey)>0){

                sortedArray[j+1]= sortedArray[j];
                j--;
            }

            sortedArray[j+1]= testKey;

        }
        return sortedArray;
    }
    }








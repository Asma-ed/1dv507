package binheab;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class BinaryIntHeapTest {

    BinaryIntHeap emptyHeap;
    BinaryIntHeap nonEmptyHeap;


    @BeforeEach
    void setUp() {

        Random r = new Random();

        // inserting 10 random numbers that have value less than 10
        nonEmptyHeap = new BinaryIntHeap();
        for(int i = 0 ; i<10 ; i++){

            nonEmptyHeap.insert(r.nextInt(10));
        }

        emptyHeap = new BinaryIntHeap();

    }

    @Test
    void testInsertPutHighestNumberOnTop () {

        nonEmptyHeap.insert(15);
        assertEquals(15,nonEmptyHeap.pullHighest());

    }

    @Test
    void testInsertDoNotPutLowNumberOnTop () {

        nonEmptyHeap.insert(-15);
        assertNotEquals(-15,nonEmptyHeap.pullHighest());

    }

    @Test
    void testInsertIncreaseSize() {

       // nonEmptyHeap size is 10

        int sizeAfterInsert = 11;
        nonEmptyHeap.insert(15);

        assertEquals(sizeAfterInsert,nonEmptyHeap.size());

    }

    @Test
    void testPullHighestOnEmptyHeap() {

        // pullHighest on an empty heap will throw NullPointerException

        boolean result = false;

        try {
            emptyHeap.pullHighest();
        }
        catch ( NullPointerException e){

            result = true;
        }

        assertTrue(result);
    }
    @Test
    void testPullHighestOnNonEmptyHeap() {

        // pullHighest will return numbers in  a descending order

        int [] testDescendingOrderSortedArray = new int[10];
        BinaryIntHeap testHeap = new BinaryIntHeap();
        for(int i = 0; i<15 ; i++){
            testHeap.insert(i);
            testDescendingOrderSortedArray[i]=9-i;
        }

        for (int j = 0 ; j<10 ; j++){

            assertEquals(testDescendingOrderSortedArray[j],testHeap.pullHighest());
        }

    }

    @Test
    void testPullHighestOnNonEmptyHeapDecreaseSize() {

        // nonEmptyHeap size is 10
        int heapSize = nonEmptyHeap.size();
        nonEmptyHeap.pullHighest();
        assertEquals(heapSize-1,nonEmptyHeap.size());


    }


    @Test
    void testIsEmptyOnNonEmptyHeap() {

        assertFalse(nonEmptyHeap.isEmpty());
    }

    @Test
    void testIsEmptyOnEmptyHeap() {

        assertTrue(emptyHeap.isEmpty());
    }
}
package binheab;


public class BinaryIntHeap {

    private int[] heapArray;
    private int size;

    public BinaryIntHeap() {
        heapArray = new int[8];
        size = 0;
    }

    public void insert(int n){

        if(size==0){

            heapArray[size]= n;
            size++;
        }

        else if (size>0){
            if (size==heapArray.length){
                resizeHeapArray();
            }
            heapArray[size]= n;
            checkParent(size);
            size++;


        }

    }

    public int pullHighest(){

        int result = heapArray[0];

        heapArray[0]= heapArray[size-1];
        size--;
        checkChildren(0);
        return result;





    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){

        if (size == 0){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[ ");
         if (isEmpty()){
             return "Heap is empty";
         }
        for (int i = 0; i< size ; i++){

            builder.append(heapArray[i]+", ");
        }
        builder.append("]");
        return builder.toString();
    }

    private void checkChildren(int i) {
        int leftChildPos = (i*2)+1;
        int rightChildPos = (i*2)+2;

        if(!haveChildren(i)){
            return;
        }


        int leftChild = heapArray[leftChildPos];
        int rightChild = heapArray[rightChildPos];
        int parent = heapArray[i];
        if(parent>= leftChild && parent>=rightChild){
            return;
        }
        else {

            if(rightChild<= leftChild){


                heapArray[i] = leftChild;
                heapArray[leftChildPos] = parent;
                checkChildren(leftChildPos);

            }

            else {


                heapArray[i] = rightChild;
                heapArray[rightChildPos] = parent;
                checkChildren(rightChildPos);
            }

        }
    }

    private boolean haveChildren(int i) {
        boolean haveChild = false;
        if (i == size-1){
            return false;
        }
        else if(i*2+1<=size-1 || i*2+2<=size-1){
          haveChild = true;
        }
        return haveChild;
    }


    private void checkParent(int i) {
        int parentIndex= (i-1)/2;

        if(i==0){
            return;
        }

        else if (heapArray[i]<= heapArray[parentIndex]){
            return;

        }

        else {

            int temp = heapArray[parentIndex];
            heapArray[parentIndex]= heapArray[i];
            heapArray[i]= temp;
            checkParent(parentIndex);



        }

    }

    private void resizeHeapArray() {

         int[] temp = heapArray;
         heapArray = new int[temp.length*2];
        for (int i = 0; i < size; i++) {
            heapArray[i]= temp[i];
        }

    }


}

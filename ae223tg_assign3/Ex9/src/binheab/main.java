package binheab;

public class main {
    public static void main(String[] args) {
        BinaryIntHeap b = new BinaryIntHeap();
        b.insert(3);
        b.insert(0);
        b.insert(1);
        b.insert(6);
        b.insert(4);
        b.insert(3);
        b.insert(2);
        b.insert(29);

        System.out.println(b);

        b.pullHighest();
        System.out.println(b);

    }
}

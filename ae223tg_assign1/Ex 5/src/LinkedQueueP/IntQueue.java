package LinkedQueueP;

/**
 * The interface Int queue.
 * Queue is a special type of linear table,
 * it only allows deleting operations at the front of the table and inserting operations in the back of the table.
 * The LinkedList class implements a queue interface, so we can use LinkedList as a queue.
 * @author Asmaa Eiz Eddin
 * @version 1.1
 * @since 2020-08-01
 */
public interface IntQueue extends Iterable<Integer> {
    /**
     * Queue size.
     *
     * @return Size of the queue.
     */
    public int size();                     // current queue size

    /**
     * Check if the queue is empty.
     *
     * @return True if queue is empty.
     */
    public boolean isEmpty();              // true if queue is empty

    /**
     * Add element at end of queue
     *
     * @param element the element at the end of the queue.
     */
    public void enqueue(int element);   // add element at end of queue

    /**
     * Return and remove first element.
     *
     * @return first element of queue.
     */
    public int dequeue();               // return and remove first element.

    /**
     * Return (without removing) first element.
     *
     * @return Return first element of queue.
     */
    public int first();                 // return (without removing) first element

    /**
     * Return (without removing) last element.
     *
     * @return last element of queue.
     */
    public int last();                  // return (without removing) last element

    /**
     * Return a string representation of the queue content
     *
     * @return  The queue content.
     */
    public String toString();              // return a string representation of the queue content

}
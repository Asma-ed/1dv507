package LinkedQueueP;

import java.util.Iterator;

/**
 * {@code LinkedQueue} The type Linked queue,represents a linked implementation of a queue.
 * provides an implementation of the Queue interface.
 * @author Asmaa Eiz Eddin
 * @version 1.1
 * @since 2020-08-01
 */
public class LinkedQueue implements IntQueue {
    /**
     * Initializes an empty queue.
     */
    public LinkedQueue() {
    }

    /**
     * {@link Node } Head of the linked queue.
     */
    private Node head;


    /**
     * {@link Node } Tail of the linked queue.
     */
    private Node tail;
    /**
     * {@link int} Size of the linked queue.
     */
    private int size = 0;

    /**
     * Function to return the size of the linked queue.
     *
     * @return Integer wich represents the size of the linked queue.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Function to check if linked queue is empty.
     *
     * @return True if queue is empty.
     */
    @Override
    public boolean isEmpty() {
        if(size==0){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Function to add element at end of queue.
     *
     * @param element the element at the end of the queue.
     */
    @Override
    public void enqueue(int element) {

        if(isEmpty()){
            Node node = new Node(element);
            head= node;
            tail = head;
            size++;
        }
        else{
            Node node = new Node(element);
            tail.nextN = node;
            tail = tail.nextN;
            size++;

        }

    }

    /**
     * Function to remove element at the front of the linked queue.
     *
     * @return First element of queue.
     */
    @Override
    public int dequeue() {
       if (!isEmpty()){

           int temp = head.data;
           head= head.nextN;
           size--;
           return temp;
       }

       else {

           throw new NullPointerException();
       }
    }

    /**
     *
     * Function to return first element.
     *
     * @return Return first element of queue.
     */
    @Override
    public int first() {
        if (!isEmpty()){

            return head.data;

        }

        else {
            throw new NullPointerException();
        }
    }

    /**
     * Function to return last element.
     *
     * @return Return last element of queue (tail).
     */
    @Override
    public int last() {
        if (!isEmpty()){

            return tail.data;

        }

        else {
            throw new NullPointerException();
        }
    }

    /**
     * Return an iterator over the elements in the linked queue.
     * @return iterator over the elements in the linked queue.
     */
    @Override
    public Iterator<Integer> iterator() {
        return new LinkedIntQueueIterator();
    }


    /**
     * linked list node
     *
     */
    private class Node{

        private int data;
        private Node nextN;

        /**
         * Instantiates a new Node.
         *
         * @param data the data (element).
         */
        public Node(int data){

            this.data = data;
        }
    }


    /**
     * A class which implements Iterator interface to iterate over elements.
     */
    private class LinkedIntQueueIterator implements Iterator<Integer> {
        private Node node = null;
        public Integer next() {

            if (!hasNext()) {
            throw new NullPointerException();
        }
            if (node == null) {
                node = head;
            }

            else {
                node = node.nextN;
            }
            return node.data;

        }

        /**
         * check linked queue to iterate over elements.
         *
         * @return True if the queue contains more elements.
         */
        public boolean hasNext() {

            if (node == null) {
            return head != null;

        } else
            return node.nextN != null;

        }

    }

    /**
     * Return a string representation of the queue content
     *
     * @return  The queue content.
     */
    @Override
    public String toString() {



        StringBuilder string = new StringBuilder();
        Node node = new Node(0);
        node = head;
        string.append("{");
        while (node != null) {
            string.append(" " + node.data);
            node = node.nextN;
        }
        string.append(" }");
        return string.toString();
    }

}

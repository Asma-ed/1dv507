package LinkedQueueP;

import java.util.Iterator;

/**
 * The type Queue main implementation.
 * @author Asmaa Eiz Eddin
 * @version 1.1
 * @since 2020-08-01
 */
public class QueueMain {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
    LinkedQueue linkedQueue = new LinkedQueue();

        for (int i = 0; i <= 15; i++) {
            linkedQueue.enqueue(i);
        }


        System.out.println("The Queue : " +linkedQueue.toString());
        System.out.println("Queue size: " +linkedQueue.size());
        System.out.println("First integer: " +linkedQueue.last());
        System.out.println("Remove and Return first Integer from the queue: " +linkedQueue.dequeue());
        System.out.println("Queue size :  " +linkedQueue.size());
        System.out.println("First integer in the queue : " +linkedQueue.first());
        System.out.println("Last integer in the queue : " +linkedQueue.last());
        System.out.println("Remove and Return first Integer: " +linkedQueue.dequeue());
        System.out.println("The Queue : " +linkedQueue.toString());
        System.out.println("The Queue Iterator : " );
        Iterator i =linkedQueue.iterator();
        while (i.hasNext()) {

            System.out.print(i.next()+" ");

        }
    }
}

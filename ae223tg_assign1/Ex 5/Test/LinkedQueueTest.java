import LinkedQueueP.LinkedQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.lang.NullPointerException;

import static org.junit.jupiter.api.Assertions.*;

class LinkedQueueTest {
    LinkedQueue integerLinkedQueue;
    LinkedQueue emptyIntegerLinkedQueue;


    @BeforeEach
    void setUp() {
        integerLinkedQueue = new LinkedQueue();
        integerLinkedQueue.enqueue(11);
        integerLinkedQueue.enqueue(22);
        integerLinkedQueue.enqueue(33);

        emptyIntegerLinkedQueue = new LinkedQueue();

    }

    @Test
    void testSizeLinkedQueue() {

        int actualSize =  integerLinkedQueue.size();
        int expectedSize = 3;
        assertEquals(expectedSize,actualSize);


    }

    @Test
    void testSizeEmptyLinkedQueue() {

        int actualSize = emptyIntegerLinkedQueue.size();

        assertEquals(0,actualSize);

    }





    @Test
    void testIsEmptyForNonEmptyLinkedQueue() {

        assertFalse(integerLinkedQueue.isEmpty());

    }

    @Test
    void testIsEmptyForEmptyLinkedQueue() {

        assertTrue(emptyIntegerLinkedQueue.isEmpty());
    }





    @Test
    void testEnqueueEmptyLinkedQueue() {



        // Enqueue when queue is empty
        emptyIntegerLinkedQueue.enqueue(1);
        assertEquals(1,emptyIntegerLinkedQueue.first());
        //Size becomes 1

        assertEquals(1,emptyIntegerLinkedQueue.size());

    }

    @Test
    void testEnqueueNonEmptyLinkedQueue() {

        LinkedQueue testQueue = new LinkedQueue();

        // First element don't change and only the last one changes
        for (int i = 0 ; i<6;i++){
            testQueue.enqueue(i);
            assertEquals(i,testQueue.last());
            assertEquals(0,testQueue.first());
        }

        //Size becomes 6 after enqueuing 6 elements

        assertEquals(6,testQueue.size());



    }




    @Test
    void testDequeueNonEmptyLinkedQueue() {

        LinkedQueue testQueue = new LinkedQueue();

        for (int i = 0 ; i<6;i++){
            testQueue.enqueue(i);
        }

        // First in first out
        for (int i = 0 ; i<6;i++){
            assertEquals(i,testQueue.dequeue());
        }




        //Size becomes 0 after dequeuing 6 elements

        assertEquals(0,testQueue.size());


    }



    @Test
    void testDequeueEmptyLinkedQueue() {
        boolean test = false;
        try {
            emptyIntegerLinkedQueue.dequeue();
        }
        catch ( NullPointerException e){
            test =true;

        }

        assertTrue(test);
    }





    @Test
    void testFirstEmptyLinkedQueue() {
        boolean test = false;
        try {
            emptyIntegerLinkedQueue.first();
        }
        catch ( NullPointerException e){
            test =true;

        }

        assertTrue(test);

    }

    @Test
    void testFirstNonEmptyLinkedQueue() {

        assertEquals(11,integerLinkedQueue.first());
    }


    @Test
    void testFirstOneElementLinkedQueue() {

        LinkedQueue testQueue = new LinkedQueue();

        testQueue.enqueue(1);

        assertEquals(1,testQueue.first());

    }





    @Test
    void testLastEmptyIntegerLinkedQueue() {
        boolean test = false;
        try {
            emptyIntegerLinkedQueue.last();
        }
        catch ( NullPointerException e){
            test =true;

        }

        assertTrue(test);

    }

    @Test
    void testLastNonEmptyIntegerLinkedQueue() {
        LinkedQueue testQueue = new LinkedQueue();

        //  The Last element changes
        for (int i = 0 ; i<6;i++){
            testQueue.enqueue(i);
            assertEquals(i,testQueue.last());

        }

    }

    @Test
    void testLastOneElementLinkedQueue() {

        LinkedQueue testQueue = new LinkedQueue();

        testQueue.enqueue(1);

        assertEquals(1,testQueue.last());

    }




    @Test
    void testIteratorNonEmptyLinkedQueue() {



        LinkedQueue testQueue = new LinkedQueue();

        Iterator iterator = testQueue.iterator();
        for (int i = 0 ; i<6;i++){
            testQueue.enqueue(i);
            while (iterator.hasNext()) {
                assertEquals(i, iterator.next());
            }

        }



    }

    @Test
    void testIteratorEmptyLinkedQueue() {

        Iterator iterator = emptyIntegerLinkedQueue.iterator();
        assertFalse(iterator.hasNext());

        boolean test = false;
        try {
            iterator.next();
        }
        catch ( NullPointerException e){
            test =true;

        }

        assertTrue(test);


    }
}
package Ferry;

import java.util.ArrayList;

public class FerrySystem implements Ferry {

    private static final int MAX_PASSENGERS_CAPACITY = 200;
    private static final int MAX_VEHICLE_CAPACITY = 200;

    private int numberOfPassengers;
    private int numberOfVehicles;
    private int earnedMoney;
    private ArrayList<Vehicle> vehicles;
    private ArrayList<Passenger> passengers;

    public FerrySystem(){

        numberOfPassengers = 0;
        numberOfVehicles = 0;
        earnedMoney = 0;
        vehicles = new ArrayList<>();
        passengers= new ArrayList<>();
    }


    /**
     * @return Number of passengers on board
     */
    @Override
    public int countPassengers() {
        return numberOfPassengers;
    }

    /**
     * @return Almost how many vehicles are on board in car unit
     */
    @Override
    public int countVehicleSpace() {

        return (int) numberOfVehicles/5;
    }

    /**
     * @return the total cost of the passengers and vehicles on board
     */
    @Override
    public int countMoney() {
        return earnedMoney;
    }


    @Override
    public Vehicle[] getAllVehicles() {
        return (Vehicle[]) vehicles.toArray();
    }

    @Override
    public void embark(Vehicle v) {

        if (checkVehicle(v)) {
            if (hasSpaceFor(v)) {
                vehicles.add(v);
                numberOfVehicles += v.getSize();
                numberOfPassengers+=v.numberOfPassengers;
                earnedMoney += v.getCost();

                for (Passenger p : v.passengers){
                    passengers.add(p);
                }


            } else {

                System.err.println("There is't enough room for a " + v.getType());

            }
        }

        else {
            System.err.println("Ferry.Vehicle is already on board");
        }
    }

    @Override
    public void embark(Passenger p) {



        if (hasRoomFor(p)) {
            passengers.add(p);
            numberOfPassengers++;
            earnedMoney+=p.getCost();
        }

        else {

            System.err.println("There is't enough room for a passenger");

        }

    }

    @Override
    public void disembark() {

        vehicles.clear();
        passengers.clear();
        numberOfPassengers = 0;
        numberOfVehicles = 0;

    }

    /**
     * @param v
     *  Checks if there is room for a vehicle and its passengers on board
     */
    @Override
    public boolean hasSpaceFor(Vehicle v) {
        if(numberOfVehicles+v.getSize() <= MAX_VEHICLE_CAPACITY && countPassengers()+v.getNumberOfPassengers()<=MAX_PASSENGERS_CAPACITY ){

            return true;
        }

        else {return false;}
    }

    @Override
    public boolean hasRoomFor(Passenger p) {
        if (numberOfPassengers + 1 <= MAX_PASSENGERS_CAPACITY){

            return true;
        }
        else {
        return false;}
    }


    /**
     * @return number of cars on board
     */
    private int numberOfCars(){

        int temp = 0;
        for (Vehicle v : vehicles){

            if (v.getType().equals("car")){
                temp++;
            }
        }
        return temp;
    }


    /**
     * @return number of bikes on board
     */
    private int numberOfBicycle(){

        int temp = 0;
        for (Vehicle v : vehicles){

            if (v.getType().equals("bicycle")){
                temp++;
            }
        }
        return temp;
    }

    /**
     * @return number of buses on board
     */
    private int numberOfBuses(){

        int temp = 0;
        for (Vehicle v : vehicles){

            if (v.getType().equals("buss")){
                temp++;
            }
        }
        return temp;
    }


    /**
     * @return number of Lorries on board
     */
    private int numberOfLorries(){

        int temp = 0;
        for (Vehicle v : vehicles){

            if (v.getType().equals("lorrie")){
                temp++;
            }
        }
        return temp;
    }

    /**
     * Checks if the vehicle already embarked
     *
     */
    private boolean checkVehicle(Vehicle v){
         boolean temp = true;
        for (Vehicle vehicle : vehicles){
            if(vehicle.getId().equals(v.getId())){

                temp = false;
            }
        }

        return temp;
    }


    /**
     * @return List of passengers on board
     */

    private String listOfPassengers() {
        StringBuilder s = new StringBuilder();
        s.append("List of passengers \n");
        for (Passenger p : passengers) {
            s.append(p);
        }
        return s.toString();
    }



    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder("Welcome to our Ferry.Ferry\non board we have\n");
        builder.append(numberOfPassengers+" Passenger\n");
        builder.append(numberOfBicycle()+" Cars\n");
        builder.append(numberOfBicycle()+" Bicycle\n");
        builder.append(numberOfBuses()+" Buses\n");
        builder.append(numberOfLorries()+" Lorries\n");
        builder.append("-----------------------------\n");
        builder.append("Money Earned on this Trip is : "+ countMoney()+"$\n");
        builder.append("List of all the Passengers : " + listOfPassengers() );

        return builder.toString();

    }
}

package Ferry;

public class Buss extends Vehicle{





    private static final int PASSENGER_COST = 15;

    public Buss(String id) {
        super();


        vehicleId= id;
        type= "buss";
        size=20;


        maxPassengersCapacity = 20;

        cost = 200;



    }

    /* All the methods that help to get mor information are called form the super class (Ferry.Vehicle) */



    /**
     * @return The total cost of the vehicle with its passengers if there is any
     */
    @Override
    public int getCost() {



        return cost+(numberOfPassengers*PASSENGER_COST);
    }



}

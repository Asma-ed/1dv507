package Ferry;

import java.util.ArrayList;

public class Vehicle {






    protected String vehicleId;

    protected int size;
    protected String type;
    protected int maxPassengersCapacity;
    protected int numberOfPassengers;
    protected int cost;
    protected ArrayList<Passenger> passengers;

    /**

     * constructor For the vehicle Class
     */
   protected Vehicle() {


        passengers= new ArrayList<>();
        numberOfPassengers = 0;


    }

    /**
     * @return The size of the vehicle
     */
   protected int getSize() {
        return size;
    }


    /**
     * @return The type of the vehicle (bicycle,car,buss,lorrie)
     */
   protected String getType() {

        return type;
    }

    /**
     * @return cost of the vehicle
     */
   protected int getCost() {
        return cost;
    }


    /**
     * @return the vehicle id
     */
   protected String getId() {

        return vehicleId;
    }

    /**
     * @return number of passengers in the vehicle
     */
   protected int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    /**
     * Add a passenger to vehicle if it's possible
     */
   protected void addPassenger(Passenger p){

        if (numberOfPassengers< maxPassengersCapacity){

            try {
                passengers.add(p);
                numberOfPassengers++;
            }

            catch (Exception e){
                throw new NullPointerException("the vehicle is full ");

            }

        }

        else {
            System.err.println("You can't that much passengers in the "+ type );


        }

    }


    /**
     * @return the list of passengers in the vehicle
     */
   protected ArrayList<Passenger> getPassengers() {
        return passengers;
    }


    @Override
   public String toString() {
        return "Ferry.Vehicle{" +
                "vehicleId='" + vehicleId + '\'' +
                ", type=" + type +
                ", numberOfPassengers=" + numberOfPassengers +
                '}';
    }

}

package Ferry;

public class Passenger {
    private static final int COST = 25;


private String name;
private String passengerId;

public Passenger(String passengerName,String passengerId){

    name = passengerName;
    this.passengerId = passengerId;
}

    /**
     * @return Cost og the passenger
     */
    public int getCost() {

        return COST;
    }

    /**
     * @return passenger name
     */
    public String getName() {
        return name;
    }

    /**
     * @return passenger id
     */
    public String getPassengerId() {
        return passengerId;
    }

    @Override
    public String toString() {
        return "\n name='" + name + '\'' +
                ", Id='" + passengerId + '\''
                ;
    }
}

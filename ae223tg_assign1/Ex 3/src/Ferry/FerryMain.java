package Ferry;

public class FerryMain {

    public static void main(String[] args) {

        Ferry ferry = new FerrySystem();


        Passenger p1 = new Passenger("William", "W1");
        Passenger p2 = new Passenger("Carl", "C1");
        Passenger p3 = new Passenger("George", "G1");
        Passenger p4 = new Passenger("Kathrine", "K1");
        Passenger p5 = new Passenger("James", "J1");
        Passenger p6 = new Passenger("John","J2");
        Passenger p7 = new Passenger("Waldo","W2");

        // Embark passengers without vehicle

        for (int i = 0; i < 10; i++) {
            ferry.embark(p7);
        }

        // Vehicles
        Vehicle car = new Car("ABC");


        Vehicle bicycle = new Bicycle("EFG",p1);



        Vehicle lorry = new Lorrie("HIJ");



        Vehicle bus = new Buss("KLM");


        // Test to embark empty vehicles

        ferry.embark(lorry);
      ferry.embark(car);

        //   Test to embarking vehicles with 1 passenger
        int i = 1;
        while (ferry.hasSpaceFor(lorry)) {
            Bicycle bike = new Bicycle("BCB"+i,p2);

            ferry.embark(bike);
            i++;
        }


        Car car2 = new Car("SSS");
      car2.addPassenger(p6);
        ferry.embark(car2);


        // Test adding more passengers in vehicle

        for (int j = 0; j < 20; j++) {
            bus.addPassenger(p1);
        }
        ferry.embark(bus);

        // Filling the rest of the ferry with cars
        int c = 1;
        while (ferry.hasSpaceFor(car)) {
            Car car1 = new Car("LBC"+c);
            car1.addPassenger(p2);
            car1.addPassenger(p1);
            ferry.embark(car1);
            c++;
        }

        // Test Embarking duplicates
        // f.embark(bicycle);
        // f.embark(lorry);
        // f.embark(car);
        // f.embark(bus);

        System.out.println("Has Room For Ferry.Passenger: " + ferry.hasRoomFor(p5));


        System.out.println("Has Space For Ferry.Vehicle: " + ferry.hasSpaceFor(bicycle));


        System.out.println("Total Passengers: " + ferry.countPassengers());


        System.out.println("Total Money: " + ferry.countMoney() + " $");


        System.out.println("Used Parking Space: " + ferry.countVehicleSpace());


        System.out.println(ferry.toString());


        ferry.disembark();
        System.out.println(".............................................\n");
        System.out.println("\nAfter Disembark:");
        System.out.println(ferry.toString());
        System.out.println("Total Money: " + ferry.countMoney() + " $");



    }

    }




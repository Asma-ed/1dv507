package Ferry;

public class Lorrie extends Vehicle {


    private static final int PASSENGER_COST = 20;

    public Lorrie(String id) {
        super();

        vehicleId= id;
        type="lorrie";
        size=40;


        maxPassengersCapacity = 2;

       cost = 300;

    }


    /* All the methods that help to get mor information are called form the super class (Ferry.Vehicle) */



    /**
     * @return The total cost of the vehicle with its passengers if there is any
     */
    @Override
    public int getCost() {



        return cost+(numberOfPassengers*PASSENGER_COST);
    }





}

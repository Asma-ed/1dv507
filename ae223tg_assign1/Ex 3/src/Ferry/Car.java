package Ferry;

public class Car extends Vehicle {

    private static final int PASSENGER_COST = 20;


    /**
     * @param id
     * Constructor to create a car with the right specification
     */
    public Car(String id) {
        super();

        vehicleId= id;
        type="car";
        size=5;


        maxPassengersCapacity = 4;

        cost = 100;


    }


    /* All the methods that help to get mor information are called form the super class (Ferry.Vehicle) */


    /**
     * @return The total cost of the vehicle with its passengers if there is any
     */
    @Override
    protected int getCost() {
        return cost+(PASSENGER_COST*numberOfPassengers);
    }
}

package Ferry;

public class Bicycle extends Vehicle {



    public Bicycle(String id,Passenger p) {
        super();

        vehicleId = id;
        type="bicycle";
        size=1;
        numberOfPassengers=0;
        cost = 40;
        maxPassengersCapacity=1;

        addPassenger(p);


    }


    /* All the methods that help to get mor information are called form the super class (Ferry.Vehicle) */


}

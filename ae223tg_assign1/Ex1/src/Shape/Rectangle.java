package Shape;

public class Rectangle extends Shape {


    protected int base;
    protected int height;

    /**
     * @param shapeName
     * @param base of a rectangle
     * @param height of a rectangle
     *
     * constructor for rectangle class that take the shape name and call the parent constructor for this class which is Shape,
     * and also takes in the base and the height of the rectangle and assign its value to the fields base and height to be used in other methods.
     */
    public Rectangle(String shapeName, int base, int height ) {
        super(shapeName);
        this.base= base;
        this.height = height;

    }


    /**
     * @return area
     * this method is an overridden function from the parent class shape used to calculate the area of the rectangle using the base and height fields .
     */
    @Override
    public double getArea() {
       double area = base*height;

       return area;
    }

    /**
     * @return Perimeter
     *
     * this method is an overridden function from the parent class shape used to calculate the perimeter of the rectangle.
     */
    @Override
    public double getPerimeter() {
       double perimeter = (2*base)+(2*height);

       return perimeter;
    }
}

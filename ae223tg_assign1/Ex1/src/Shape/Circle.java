package Shape;

public class Circle extends Shape {

    private int radius;


    /**
     * @param shapeName
     * @param radius
     *
     * constructor for circle class that take the shape name and call the parent constructor for this class which is Shape,
     * and also take in the radius of the circle and assign its value to the field radius to be used in other methods.
     */
    public Circle(String shapeName, int radius) {
        super(shapeName);

        this.radius = radius;
    }

    /**
     * @return area
     *
     * this method is an overridden function from the parent class shape used to calculate the area of the circle.
     */
    @Override
    public double getArea() {

        double area = (Math.pow(radius,2))*Math.PI;

        return area;
    }

    /**
     * @return perimeter
     * this method is an overridden function from the parent class shape used to calculate the perimeter of the circle.
     */
    @Override
    public double getPerimeter() {
        double perimeter = (radius*2)*Math.PI;

        return perimeter;
    }
}

package JavaCollection;

public class ArrayIntList extends AbstractIntCollection implements IntList {

   private int indexN=0;

    @Override
    public void add(int n) {
        if (size >= values.length) {

            resize();


        }
        values[indexN]=n;
        indexN++;
        size++;


    }

    @Override
    public void addAt(int n, int index) throws IndexOutOfBoundsException {

        if (size >= values.length) {

            resize();


        }

        if (checkIndex(index,indexN+1)){


            for (int i = indexN - 1; i >= index; i--) {
                values[i + 1] = values[i]; // shift the element to next position
            }
            values[index] = n;
            size++;
            indexN++;
        } else
            throw new IndexOutOfBoundsException();
    }






    @Override
    public void remove(int index) throws IndexOutOfBoundsException {

        if(checkIndex(index,size)){

            for(int i = index ; i<indexN-1;i++){


                values[i]= values[i+1];

            }

            size--;
            indexN--;

        }

     else{
            throw new IndexOutOfBoundsException();
}

    }

    @Override
    public int get(int index) throws IndexOutOfBoundsException {
       if(checkIndex(index,indexN)){

           return values[index];

       }

       else throw new IndexOutOfBoundsException();
    }

    @Override
    public int indexOf(int n) {

        for (int i = 0; i < indexN; i++) {
            if (values[i] == n)
                return i;
        }
        return -1;
    }


    public int size(){

        return size;
    };

    public boolean isEmpty(){
        if(size==0){
            return true;
        }
        else {
            return false;
        }
    }
}

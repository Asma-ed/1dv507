package JavaCollection;

public class ArrayIntStack extends AbstractIntCollection implements IntStack {

    @Override
    public void push(int n) {
        if (size == values.length) {
            resize();
        }
        values[size] = n;
        size++;

    }


    @Override
    public int pop() throws IndexOutOfBoundsException {
        int temp;
        if (!isEmpty()) {

            temp = values[size - 1];
            size--;
            return temp;
        }


        else {

            throw new IndexOutOfBoundsException("The stack is empty");
        }


    }


    @Override
    public int peek() throws IndexOutOfBoundsException {
        if (!isEmpty()){
            return values[size - 1];}
        else {
            throw new IndexOutOfBoundsException("Stack is empty");
        }
    }


}

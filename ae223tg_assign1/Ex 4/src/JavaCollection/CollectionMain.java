package JavaCollection;

import java.util.Iterator;

public class CollectionMain {

    public static void main(String[] args) {

        ArrayIntList arrayIntList = new ArrayIntList();
        System.out.println("Integer list ");

        System.out.println("Check if list is empty when bo element was added : " + arrayIntList.isEmpty());

        for (int i = 0; i < 19; i++) {
            arrayIntList.addAt(i, 0);
        }


        System.out.println("Printing List after adding 19 elements: " + arrayIntList.toString());
        System.out.println("Size after adding 19 element : " + arrayIntList.size());
        System.out.println("Get element with index 5 :  " + arrayIntList.get(5));
        System.out.println("Index of element: " + arrayIntList.indexOf(5));

        for (int i = 4; i< 8; i++) {
            arrayIntList.remove(i);
        }
        arrayIntList.add(0);
        System.out.println("Printing List after removing 3 elements: " + arrayIntList.toString());
        System.out.println("Size after removing 3 elements: " + arrayIntList.size());

        Iterator<Integer> i = arrayIntList.iterator();
        System.out.print("Iterator:  ");
        while (i.hasNext()) {
            System.out.print(+ i.next() + "  ");
        }
        System.out.println("\n...............................................");


        ArrayIntStack stack = new ArrayIntStack();

        for (int j = 0; j < 10; j++) {
            stack.push(j);
        }

        System.out.println("Integer stack ");

        System.out.println("print stack: " + stack.toString());
        System.out.println("Peek: " + stack.peek());
        System.out.println("Size: " + stack.size());
        System.out.println("Pop: " + stack.pop());
        System.out.println("Stack Size: " + stack.size());
        System.out.println("Pop: " + stack.pop());
        System.out.println("Peek: " + stack.peek());
        System.out.println("Stack Size after removing an element : " + stack.size());
        System.out.println("Check if Empty: " + stack.isEmpty());

        Iterator<Integer> it = stack.iterator();
        System.out.print("Iterator:  ");
        while (it.hasNext()) {
            System.out.print(it.next() + "  ");
        }



    }

}

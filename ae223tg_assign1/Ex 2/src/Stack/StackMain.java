package Stack;

public class StackMain {



   public static void testPushAndSize(){
        Stack stack = new Stack();
        System.out.println("Stack size before pushing anything : "+stack.size());
        stack.push("Apple");
        stack.push("Banana");
        stack.push("Orange");

        System.out.println("Stack size after pushing three elements : "+stack.size());
        System.out.println("Stack content : ");
        System.out.println(stack);


    }

    private void testPopAndSize(){

        Stack stack = new Stack();

        stack.push("Apple");
        stack.push("Banana");
        System.out.println("Stack size before poping anything : "+stack.size());
        stack.pop();
        stack.pop();

        System.out.println("Stack size after poping all the elements : "+stack.size());
        System.out.println("Stack content : ");


    }

    private void testPeek(){

        Stack stack = new Stack();

        stack.push("Apple");
        stack.push("Banana");

        System.out.println();
    }

    public static void main(String[] args) {

        System.out.println("Test Push and size for Stack ");

        Stack stack = new Stack();
        System.out.println("Stack size before pushing anything : "+stack.size());
        stack.push("Apple");
        stack.push("Banana");
        stack.push("Orange");

        System.out.println("Stack size after pushing three elements : "+stack.size());
        System.out.println("Stack content : ");
        System.out.println(stack);

        System.out.println("........................................ \n ");
        System.out.println("Test pop and size for Stack ");

        Stack stack1 = new Stack();

        stack1.push("Apple");
        stack1.push("Banana");
        System.out.println("Stack size before poping anything : "+stack1.size());
        stack1.pop();
        stack1.pop();

        System.out.println("Stack size after poping all the elements : "+stack1.size());



        System.out.println("........................................ \n ");
        System.out.println("Test peek for Stack ");

        Stack stack2 = new Stack();

        stack2.push("Apple");
        stack2.push("Banana");
        System.out.println("Stack size before peeking  : "+stack2.size());
        System.out.println("Stack content : ");
        System.out.println(stack2.toString());

        System.out.println("The top element in the stack: "+stack2.peek());
        System.out.println("Stack size after peeking  : "+stack2.size());

        System.out.println("........................................ \n ");
        System.out.println("Test pushing 100 element : ");

        Stack stack3 = new Stack();
        for (int i = 0 ; i<100 ; i++){
            stack3.push(i+"");
        }
        System.out.println("Stack size after pushing 100 element : "+stack3.size());





    }

}

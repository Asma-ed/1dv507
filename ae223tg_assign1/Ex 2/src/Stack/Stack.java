package Stack;

public class Stack implements StringStack {
     private int size; // Stack size
     private static final int DEFAULT_SIZE = 10; // Default size for the Array
     private String[] arr; // The Array that will contain the elements of the stack


    /**
     *  constructor For the stack class that crates an array with Default size of 10 and sets the size of the stack to 0
     */
    public Stack(){


        arr= new String[DEFAULT_SIZE];
        size= 0;
    }

    /**
     * @return The size of the stack
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @return
     * Check if the stack is empty
     */
    @Override
    public boolean isEmpty() {
        if(size == 0){

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param element
     *
     * Push a String to the stack and resize if necessary
     */
    @Override
    public void push(String element) {

        if (size >= arr.length) {

            resize();   // Resize the array that holds the elements of the stack



        }
        arr[size] = element;
        size++;

    }

    private void resize() {

        String[] temp = arr;
        arr = new String[temp.length*2];
        for (int i = 0 ; i<size ; i++){

            arr[i] = temp[i];
        }
    }

    /**
     * @return The element that was poped
     * @throws NullPointerException if the stack is empty
     *
     * Pop the last element of the stack
     */
    @Override
    public String pop() throws NullPointerException {

        if(!isEmpty()){
        String temp = arr[size-1];
        arr[size--]=null;


        return temp;

        }
        else {

        throw new  NullPointerException("The stack is empty");
        }

    }

    /**
     * @return The lst element of the stack without removing it.
     * @throws NullPointerException
     */
    @Override
    public String peek() throws NullPointerException {
        if(!isEmpty()){
        return arr[size-1];
    }
        else {
            throw new NullPointerException("The stack is empty");
        }
    }

    /**
     * Print out all the elements in the stack
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        for (int i = size-1; i>-1 ; i--){
            builder.append(arr[i]+"\n");
        }
        builder.append("}");
        return builder.toString();
    }
}

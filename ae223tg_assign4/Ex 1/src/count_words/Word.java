package count_words;

public class Word implements Comparable<Word> {

    private String word;

    public Word(String str){

        word= str.toLowerCase();
    }


    public String toString() { return word; }

    /* Override Object methods */
    public int hashCode() {

       int hash = word.hashCode();

       return Math.abs(hash);

    }
    public boolean equals(Object other) {


        if(this == other){
            return true;
        }

        else if(other==null){
            return false;
        }

        else if (other instanceof Word){

            Word word1 = (Word) other;

            if (this.hashCode() == word1.hashCode()){

                return true;
            }
        }


            return false;

    }

    /* Implement Comparable */
    public int compareTo(Word w) {

        return word.compareTo(w.word);
    }
}

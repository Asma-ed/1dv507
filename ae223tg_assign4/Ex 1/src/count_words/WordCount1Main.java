package count_words;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class WordCount1Main {
   static String filePath = "C:\\Users\\saad\\Desktop\\New folder (16)\\Assignmen4-20200821T105116Z-001\\Assignmen4\\Ex1\\src\\words.txt";

    static Scanner fileReader;
    static HashSet<Word> wordHashSet = new HashSet<Word>();
    static TreeSet<Word> wordTreeSet = new TreeSet<Word>();

    public static void main(String[] args) {

        try {
            File txtFile = new File(filePath);
            fileReader= new Scanner(txtFile);

            while (fileReader.hasNext()){

                String readWord= fileReader.next();

                Word word = new Word(readWord);

                wordHashSet.add(word);
                wordTreeSet.add(word);

            }

            System.out.println("Hash set size = " +wordHashSet.size());
            System.out.println("Tree set size = " +wordTreeSet.size());

            int numberOfWords = 1;
            System.out.println("HashTree set Iterator : ");

            Iterator<Word> it = wordTreeSet.iterator();
            while (it.hasNext()) {
                System.out.println(numberOfWords + " :" + it.next());
                numberOfWords++;
            }

        }


        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

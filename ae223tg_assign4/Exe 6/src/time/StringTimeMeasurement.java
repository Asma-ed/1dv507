package time;

public class StringTimeMeasurement {

    private final static long SECOND = 1000000000;
    private final static long MilliSECOND = 1000000;


    public static void main(String[] args) {
         /*for (int i = 0; i<=20;i++ ){
             if (i==0){
                 System.out.println("Appending long string");
             }
            if (i<=4){
                AppendingLongStrings();
            }
             if (i==5){
                 System.out.println();
                 System.out.println("Appending short string");
             }

             if (i>4 && i<=9){
                 AppendingShortStrings();
             }
             if (i==10){
                 System.out.println();
                 System.out.println(" Concatenating Long Strings");
             }
             if (i>9 && i<=14){
                 ConcatenatingLongStrings();
             }
             if (i==15){
                 System.out.println();
                 System.out.println(" Concatenating short Strings");
             }
             if (i>14 && i<=19){
                 ConcatenatingShortStrings();
             }*/
        for (int i = 0; i < 5; i++) {
            AppendingShortStrings();
        }


    }

    private static void ConcatenatingShortStrings(){

        String indexString="";

        long timerStart = System.nanoTime();
        while (System.nanoTime()-timerStart<SECOND){
            indexString = indexString+"j";
        }

        long timerEnd = System.nanoTime();

        long time = (timerEnd-timerStart)/MilliSECOND;

        System.out.println("time :" + time);
        System.out.println("length of the string : "+indexString.length());


    }

    private static void ConcatenatingLongStrings(){

        String indexString="";

        long timerStart = System.nanoTime();
        while (System.nanoTime()-timerStart<SECOND){
            indexString = indexString+"jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj";
        }

        long timerEnd = System.nanoTime();

        long time = (timerEnd-timerStart)/MilliSECOND;

        System.out.println("time :" + time);
        System.out.println("length of the string : "+indexString.length()/80);


    }
    private static void AppendingShortStrings(){

        StringBuilder builder = new StringBuilder("");

        long timerStart = System.nanoTime();
        while (System.nanoTime()-timerStart<SECOND){
            builder.append("j");
        }
        long timerEnd = System.nanoTime();
        long time = (timerEnd-timerStart)/MilliSECOND;

        // toString Time
        timerStart = System.nanoTime();
        builder.toString();
        timerEnd = System.nanoTime();
        long toStringTime = (timerEnd-timerStart);

        // final counting of total appending

        builder = new StringBuilder("");
        long finalTimeStart = System.nanoTime();
        while (System.nanoTime()-finalTimeStart<SECOND-toStringTime){
            builder.append("j");
        }
        long finalTimerEnd = System.nanoTime();

        long finalTime = ((finalTimerEnd-finalTimeStart)+toStringTime)/MilliSECOND;





        System.out.println("time :" + finalTime);
        System.out.println("length of the string : "+ builder.length());


    }
    private static void AppendingLongStrings(){

        StringBuilder builder = new StringBuilder();

        long timerStart = System.nanoTime();
        while (System.nanoTime()-timerStart<SECOND){
            builder.append("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        }
        long timerEnd = System.nanoTime();
        long time = (timerEnd-timerStart)/MilliSECOND;



        // toString Time
        timerStart = System.nanoTime();
        builder.toString();
        timerEnd = System.nanoTime();
        long toStringTime = (timerEnd-timerStart);

        // final counting of total appending

       StringBuilder stringBuilder = new StringBuilder();
        long finalTimeStart = System.nanoTime();
        while (System.nanoTime()-finalTimeStart<SECOND-toStringTime){
            stringBuilder.append("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        }
        long finalTimerEnd = System.nanoTime();

        long finalTime = ((finalTimerEnd-finalTimeStart)+toStringTime)/MilliSECOND;





        System.out.println("time :" + finalTime);
        System.out.println("length of the string : "+ stringBuilder.length()/80);


    }
}

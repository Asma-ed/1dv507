import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Snowman extends Application {

    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {


        Group root = new Group();
        Scene scene = new Scene(root ,800,500, Color.LIGHTBLUE);

        Rectangle ground = new Rectangle(800,150,Color.WHITE);
        Circle sun = new Circle(700,80,60,Color.YELLOW);
        ground.setY(350);
        Circle lowerCircle  = new Circle(400,285,80,Color.WHITE);

        Circle middleCircle  = new Circle(400,175,50,Color.WHITE);
        Circle head = new Circle(400,105,30,Color.WHITE);
        Circle leftEye = new Circle(385,100,4);
        Circle rightEye = new Circle(415,100,4);
        Line mouth = new Line(385,115,415,115);

        Circle button1 = new Circle(400,200,5,Color.BLACK);
        Circle button2 = new Circle(400,180,5,Color.BLACK);
        Circle button3 = new Circle(400,160,5,Color.BLACK);



        root.getChildren().add(ground);
        root.getChildren().add(sun);
        root.getChildren().add(lowerCircle);
        root.getChildren().add(middleCircle);
        root.getChildren().add(head);
        root.getChildren().add(leftEye);
        root.getChildren().add(rightEye);
        root.getChildren().add(mouth);
        root.getChildren().add(button1);
        root.getChildren().add(button2);
        root.getChildren().add(button3);

        stage.setTitle("Snowman");
        stage.setScene(scene);
        stage.show();
    }
}

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;

import javafx.stage.Stage;

import java.util.ArrayList;


public class CompoundInterest extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane parent = new AnchorPane();
        Scene scene = new Scene(parent , 400 , 300);
        stage.setScene(scene);
        stage.setTitle("Compound Interest");

        Label label1 = new Label("Compound Interest");
        label1.setLayoutX(14);
        label1.setLayoutY(14);
        label1.setFont(Font.font(27));

        Label startAmountLabel = new Label("Start Amount :");
        startAmountLabel.setLayoutX(14);
        startAmountLabel.setLayoutY(81);

        TextField startAmount = new TextField();
        startAmount.setLayoutX(116.0);
        startAmount.setLayoutY(77);



        Label interestLabel = new Label(" Interest :");
        interestLabel.setLayoutX(14);
        interestLabel.setLayoutY(129);

        TextField interest = new TextField();
        interest.setLayoutX(116.0);
        interest.setLayoutY(125);



        Label numOfYearsLabel = new Label("Number of years :");
       numOfYearsLabel.setLayoutX(14);
        numOfYearsLabel.setLayoutY(175);

        TextField numOfYears = new TextField();
        numOfYears.setLayoutX(116.0);
        numOfYears.setLayoutY(171);




        Label label2 = new Label("In total that will be :");
        label2.setLayoutX(23);
        label2.setLayoutY(269);


        Label result = new Label();
        result.setLayoutX(139);
        result.setLayoutY(269);


        Button calculate = new Button("Calculate");
        calculate.setLayoutX(20);
        calculate.setLayoutY(227);


        EventHandler <ActionEvent> calculation = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {


                    int principal = Integer.parseInt(startAmount.getCharacters().toString());
                    double rate = Integer.parseInt(interest.getCharacters().toString());
                    int years = Integer.parseInt(numOfYears.getCharacters().toString());

                    int amountEarned = (int) (principal * (Math.pow((1 + rate / 100), years)));
                    result.setText(amountEarned + "");
                }
                catch (Exception e){

                    System.err.println("You have entered an illegal argument");
                }
            }
        };

        calculate.setOnAction(calculation);





        parent.getChildren().add(label1);
        parent.getChildren().add(startAmountLabel);
        parent.getChildren().add(startAmount);
        parent.getChildren().add(interestLabel);
        parent.getChildren().add(interest);
        parent.getChildren().add(numOfYearsLabel);
        parent.getChildren().add(numOfYears);
        parent.getChildren().add(label2);
        parent.getChildren().add(result);
        parent.getChildren().add(calculate);




        stage.show();










    }
}

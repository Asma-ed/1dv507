package sample;

import java.util.ArrayList;
import java.util.Arrays;

public class ScoringSystem {

    int[] dice;

    public ScoringSystem(int[] dices) {
        dice = dices;
        Arrays.sort(dice);
    }


    public boolean yahtzee() {

        for (int i = 0; i < dice.length; i++) {
            int count = 0;
            for (int j = 0; j < dice.length; j++) {

                if (dice[i] == dice[j]) {
                    count++;
                }

            }
            if (count == 5) {
                return true;
            }
        }
        return false;
    }

    public boolean fourOfAKind (){
        for (int i = 0; i < dice.length; i++) {
            int count = 0;
            for (int j = 0; j < dice.length; j++) {

                if (dice[i] == dice[j]) {
                    count++;
                }

            }
            if (count == 4) {
                return true;
            }
        }
        return false;

    }

    public boolean largeStraight (){
        int count = 0;
        for (int i = 0; i < dice.length-1; i++) {

            if (dice[i]+1 != dice[i+1]) {
                    count++;
                }


            }
            if (count == 0) {
                return true;
            }

        return false;

    }

    public boolean smallStraight (){
        int count = 0;
        for (int i = 0; i < dice.length-1; i++) {

            if (dice[i]+1 != dice[i+1]) {
                count++;
            }


        }
        if (count == 1) {
            return true;
        }

        return false;

    }

    public boolean threeOfAKind (){
        for (int i = 0; i < dice.length; i++) {
            int count = 0;
            for (int j = 0; j < dice.length; j++) {

                if (dice[i] == dice[j]) {
                    count++;
                }

            }
            if (count == 3) {
                return true;
            }
        }
        return false;

    }

    public boolean pair (){
        for (int i = 0; i < dice.length; i++) {
            int count = 0;
            for (int j = 0; j < dice.length; j++) {

                if (dice[i] == dice[j]) {
                    count++;
                }

            }
            if (count == 2) {
                return true;
            }
        }
        return false;

    }

    public boolean fullHouse (){

        for (int i = 0; i < dice.length; i++) {
            int count = 0;
            for (int j = 0; j < dice.length; j++) {

                if (dice[i] == dice[j]) {
                    count++;
                }

            }
            if (count == 3) {
                int[] temp = removeElements(dice,dice[i]);
                if(temp[0]==temp[1]){
                return true;}
            }
        }
        return false;

    }


    private  int[] removeElements(int[] arr, int key)
    {

        int index = 0;
        for (int i=0; i<arr.length; i++)
            if (arr[i] != key)
                arr[index++] = arr[i];


        return Arrays.copyOf(arr, index);
    }


}




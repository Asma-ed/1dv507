package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;


import java.util.ArrayList;
import java.util.Random;


public class Yahtzee extends Application {
    ArrayList<Image> dices;
    ArrayList<ImageView> imageViews;
    Label label2;
    ImageView image1;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    ImageView image5;

    CheckBox checkbox1;
    CheckBox checkbox2;
    CheckBox checkbox3;
    CheckBox checkbox4;
    CheckBox checkbox5;

    int rolls = 0;



public Yahtzee(){

    dices = new ArrayList<>();
    imageViews = new ArrayList<>();


}


    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {



        dices.add(new Image("/sample/dice/Dice-1-b.svg.png"));
        dices.add(new Image("/sample/dice/Dice-2-b.svg.png"));
        dices.add(new Image("/sample/dice/Dice-3-b.svg.png"));
        dices.add(new Image("/sample/dice/Dice-4-b.svg.png"));
        dices.add(new Image("/sample/dice/Dice-5-b.svg.png"));
        dices.add(new Image("/sample/dice/1024px-Dice-6a-b.svg.png"));




        AnchorPane parent = new AnchorPane();
        Scene scene = new Scene(parent,429.0,308.0);


        Label label1 = new Label("Yahtzee");
        label1.setLayoutY(20);
        label1.setLayoutX(20);
        label1.setFont(new Font(27));

         image1 = new ImageView();
         imageViews.add(image1);
        image1.setFitWidth(72);
        image1.setFitHeight(68);
        image1.setLayoutX(14);
        image1.setLayoutY(82);

         image2 = new ImageView();
         imageViews.add(image2);
        image2.setFitWidth(72);
        image2.setFitHeight(68);
        image2.setLayoutX(92);
        image2.setLayoutY(82);

         image3 = new ImageView();
        imageViews.add(image3);
        image3.setFitWidth(72);
        image3.setFitHeight(68);
        image3.setLayoutX(172.0);
        image3.setLayoutY(82);

         image4 = new ImageView();
        imageViews.add(image4);
        image4.setFitWidth(72);
        image4.setFitHeight(68);
        image4.setLayoutX(255);
        image4.setLayoutY(82);

        image5 = new ImageView();
        imageViews.add(image5);
        image5.setFitWidth(72);
        image5.setFitHeight(68);
        image5.setLayoutX(335);
        image5.setLayoutY(82);



        checkbox1 = new CheckBox();
        checkbox1.setLayoutX(40);
        checkbox1.setLayoutY(170);
        checkbox1.setDisable(true);
         checkbox2 = new CheckBox();
        checkbox2.setLayoutX(118);
        checkbox2.setLayoutY(170);
        checkbox2.setDisable(true);
         checkbox3 = new CheckBox();
        checkbox3.setLayoutX(198);
        checkbox3.setLayoutY(170);
        checkbox3.setDisable(true);
         checkbox4 = new CheckBox();
        checkbox4.setLayoutX(281);
        checkbox4.setLayoutY(170);
        checkbox4.setDisable(true);
         checkbox5 = new CheckBox();
        checkbox5.setLayoutX(361);
        checkbox5.setLayoutY(170);
        checkbox5.setDisable(true);


        Button button = new Button("Roll the dice !");
        button.setLayoutY(256.0);
        button.setLayoutX(22.0);

        label2 = new Label("You have 3 rolls left.");
        label2.setLayoutY(260.0);
        label2.setLayoutX(134.0);

       randomImageView();


        EventHandler<ActionEvent> rollTheDice = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                if(rolls == 0){

                    randomImageView();

                    rolls++;
                    label2.setText("You have 2 rolls left.");

                }



                 if(rolls<4&&rolls>0){

                    checkBoxDisable(false);
                    randomImageView();
                     label2.setText("You have "+ (3-rolls) +" roll(s) left.");
                    rolls++;


                }

                if(rolls==4){

                    score();
                }





            }
        };

        button.setOnAction(rollTheDice);















        parent.getChildren().add(label1);
        parent.getChildren().add(image1);
        parent.getChildren().add(image2);
        parent.getChildren().add(image3);
        parent.getChildren().add(image4);
        parent.getChildren().add(image5);

        parent.getChildren().add(checkbox1);
        parent.getChildren().add(checkbox2);
        parent.getChildren().add(checkbox3);
        parent.getChildren().add(checkbox4);
        parent.getChildren().add(checkbox5);

        parent.getChildren().add(button);
        parent.getChildren().add(label2);




        stage.setScene(scene);
        stage.setTitle("Yahtzee");
        stage.show();



    }

    private void score() {
    int[] rolledDice = new int[5];
    int i = 0;
        for(ImageView imageView : imageViews){
            rolledDice[i]=dices.indexOf(imageView.getImage());
            i++;

        }

        ScoringSystem scoringSystem = new ScoringSystem(rolledDice);

        if (scoringSystem.yahtzee()){
            label2.setText("Yahtzee");

        }

        else if (scoringSystem.fourOfAKind()){

            label2.setText("Large straight");

        }

        else if (scoringSystem.fullHouse()){

            label2.setText("Full House");
        }

        else if (scoringSystem.threeOfAKind()){
            label2.setText("Three of a kind");
        }

        else if (scoringSystem.largeStraight()){

            label2.setText("Large straight");
        }
        else if (scoringSystem.smallStraight()){

            label2.setText("small straight");
        }
        else if (scoringSystem.pair()){

            label2.setText("Pair");
        }

    }

    private void randomImageView (){
        Random randomNum = new Random();



        for(ImageView imageView : imageViews){
            int index = imageViews.indexOf(imageView);
           if(!checkIfCanThrow(index)){
            imageView.setImage(dices.get(randomNum.nextInt(6)));}

        }

    }

    private boolean checkIfCanThrow(int index) {
    boolean checked = false;
        switch (index+1) {
            case 1:
               checked = checkbox1.isSelected();
            break;
            case 2:
                checked =  checkbox2.isSelected();
            break;
            case 3:
                checked = checkbox3.isSelected();
            break;
            case 4:
                checked =  checkbox4.isSelected();
            break;
            case 5:
                checked =  checkbox5.isSelected();
            break;


        }
        return checked;
    }


    private void checkBoxDisable(boolean status){
        checkbox1.setDisable(status);
        checkbox2.setDisable(status);
        checkbox3.setDisable(status);
        checkbox4.setDisable(status);
        checkbox5.setDisable(status);


    }



}
